//
//  CompanyStructure.swift
//  Company
//
//  Created by Renuka Javheri on 25/11/21.
//

import Foundation
import UIKit

struct Company{
    var name = ""
    var address = ""
    var image: UIImage?
}

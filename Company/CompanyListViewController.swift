//
//  ViewController.swift
//  Company
//
//  Created by Renuka Javheri on 25/11/21.
//

import UIKit

class CompanyListViewController: UIViewController, CompanyDetailsViewControllerDelegate {
    
    @IBOutlet weak var CompanyTableView: UITableView!

    var cellTapped = 0
    var companyArray = [Company]()
    var cellHeight: CGFloat = 150
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        companyArray = [Company(name: "Company 1", address: "Pune", image: UIImage(named: "Company 2")),
                        Company(name: "Company 1", address: "Mumbai", image: UIImage(named: "Company 1")),
                        Company(name: "Company 3", address: "Maxico", image: UIImage(named: "Company 2")),
                        Company(name: "Company 4", address: "USA", image: UIImage(named: "Company 1")),
                        Company(name: "Company 5", address: "Africa", image: UIImage(named: "Company 1")),
                        Company(name: "Company 6", address: "Pune", image: UIImage(named: "Company 2")),
                        Company(name: "Company 6", address: "Pune", image: UIImage(named: "Company 2"))]
        
        CompanyTableView.separatorStyle = .none
        CompanyTableView.delegate = self
        CompanyTableView.dataSource = self
    }
    
    func addCompanyDetailsViewController(_ ViewController: CompanyDetailsViewController, didAddCompany newData: Company) {
        if (newData.name != " " && newData.address != " " && newData.image != UIImage(named: " ") ){
            companyArray.append(newData)
        }
        CompanyTableView.reloadData()
    }

    @IBAction func didTappedAddNewButton(_ sender: Any) {
        let detailViewController = storyboard?.instantiateViewController(withIdentifier: String(describing: CompanyDetailsViewController.self)) as! CompanyDetailsViewController
        detailViewController.delegate = self
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}
extension CompanyListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CompanyTableViewCell.self)) as! CompanyTableViewCell
        cell.companyNameLabel.text = companyArray[indexPath.row].name
        cell.companyAddressLabel.text = companyArray[indexPath.row].address
        cell.companyImageView.image = companyArray[indexPath.row].image
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailViewController = storyboard?.instantiateViewController(withIdentifier: String(describing: CompanyDetailsViewController.self)) as! CompanyDetailsViewController
        detailViewController.companyData = companyArray[indexPath.row]
       
        navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = cellHeight
        return height
    }
    
    
}


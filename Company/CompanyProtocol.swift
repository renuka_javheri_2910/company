//
//  CompanyProtocol.swift
//  Company
//
//  Created by Renuka Javheri on 25/11/21.
//

import Foundation

protocol CompanyDetailsViewControllerDelegate: AnyObject{
 
    func addCompanyDetailsViewController(_ ViewController: CompanyDetailsViewController, didAddCompany newData: Company)
}

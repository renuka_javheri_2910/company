//
//  CompanyDetailsViewController.swift
//  Company
//
//  Created by Renuka Javheri on 25/11/21.
//

import UIKit

class CompanyDetailsViewController: UIViewController {
    @IBOutlet weak var companyDetailImageView: UIImageView!
    @IBOutlet weak var companyNameTextFeild: UITextField!
    @IBOutlet weak var companyAddressTextFeild: UITextField!
    
    weak var delegate: CompanyDetailsViewControllerDelegate?
    var companyData = Company(name: " ", address: " ", image: UIImage(named: "Company 2"))

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        companyDetailImageView.image = companyData.image
        companyNameTextFeild.text = companyData.name
        companyAddressTextFeild.text = companyData.address
        
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        companyNameTextFeild.placeholder = "Enter Company Name"
        companyAddressTextFeild.placeholder = "Enter Company Address"
    }
    

    @IBAction func didSelectImageButton(_ sender: Any) {
        showAlert()
    }
    
    
    @IBAction func didTappedAddButton(_ sender: Any) {
        let newCompany = Company(name: companyNameTextFeild.text ?? companyData.name, address: companyAddressTextFeild.text ?? companyData.address, image: companyDetailImageView.image ?? companyData.image)
        let vc = CompanyDetailsViewController()

        self.delegate?.addCompanyDetailsViewController(vc, didAddCompany: newCompany)
        //Alert For  Saving Data
        let alert = UIAlertController(title: " ", message: "Are u sure wants save Data?", preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)


        //Provide Action Handler
        let actionYes = UIAlertAction(title: "Yes", style: .default) { (action) in
            print("Data Save SuccessFully")

        //Back
        self.navigationController?.popViewController(animated: true)

        }
        alert.addAction(actionOk)
        alert.addAction(actionYes)

        present(alert, animated: true, completion: nil)
    }
    
}
//MARK:- Image Picker
extension CompanyDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {


    //Show alert to selected the media source type.
    private func showAlert() {

        let alert = UIAlertController(title: "Image Selection", message: "Please Select Company Logo", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    //get image from source type
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {

        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {

            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }

    //MARK:- UIImagePickerViewDelegate.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        self.dismiss(animated: true) { [weak self] in

            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            //Setting image to your image view
            self?.companyDetailImageView.image = image
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

}

